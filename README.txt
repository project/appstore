A feature defining fields, views, rules, services for App store distribution.

This module is based on the work described here: 
http://groups.drupal.org/node/175329
Apps are the future way of adding new functionality to Drupal websites. 
Just like mobile platforms, Drupal websites will be able to browse 
applications hosted on various stores, and download/install them locally. 
Work is being done to compose a standard for apps authoring 
(see http://community.aegirproject.org/handbook/open-app-standard-draft), 
and this feature completes the work by creating a Drupal Commerce based store. 
The store allows browsing applications, purchasing them (or getting them for 
free of course), install them remotely and manage several accounts (websites). 
In the future we envision many app stores which serve many Drupal websites, 
distributions and other apps.

This module is basically a feature defining fields, views, rules, 
services for App store distribution. This in not a standalone module, 
but a part of App store distribution, which will make a use of it 
automatically - download and install(drupal-org.make).

There is no need to download or install this feature(module) by your own!