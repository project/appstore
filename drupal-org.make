core = 7.x
api = 2

projects[ctools] = 1.1
projects[entity] = 1.0-rc3
projects[rules] = 2.2
projects[views] = 3.3
projects[features] = 1.0
projects[addressfield] = 1.0-beta3
projects[commerce] = 1.3
projects[commerce_features] = 1.0-alpha1
projects[commerce_file] = 1.0-beta3
projects[votingapi] = 2.6
projects[fivestar] = 2.0-alpha2
projects[services] = 3.1
projects[oauth] = 3.0
projects[appstore_feature] = 1.x-dev
projects[dartik] = 1.2
